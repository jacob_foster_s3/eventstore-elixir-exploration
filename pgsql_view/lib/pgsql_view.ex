defmodule PgsqlView do
  @moduledoc """
  Documentation for PgsqlView.
  """

  @doc """
  Hello world.

  ## Examples

      iex> PgsqlView.hello()
      :world

  """
  def hello do
    :world
  end
end
