defmodule PgsqlView.Persistor do
  alias PgsqlView.{EventMetadata, Repo, Processors}
  alias Ecto.Multi

  def persist(stream_name, event_number, payload, event_type) do
    Multi.new()
    |> Multi.append(update_metadata(stream_name, event_number))
    |> Multi.append(Processors.Person.process_event(payload, event_type))
    |> Repo.transaction()
  end

  defp update_metadata(stream_name, event_number) do
    Multi.new()
    |> Multi.insert_or_update(
      :event_metadata,
      EventMetadata.changeset(
        Repo.get(EventMetadata, stream_name) || %EventMetadata{},
        %{
          stream_name: stream_name,
          last_event_id: event_number
        }
      )
    )
  end
end
