defmodule PgsqlView.Processors.PersonDeleted do
  alias PgsqlView.{Person, Repo}
  alias Ecto.Multi

  def process(payload) do
    Repo.get(Person, payload["id"])
    |> delete_person(payload)
  end

  defp delete_person(_existing_person = nil, _payload), do: Multi.new()

  defp delete_person(existing_person = %Person{}, _payload) do
    Multi.new()
    |> Multi.delete(
      :delete_person,
      existing_person
    )
  end
end
