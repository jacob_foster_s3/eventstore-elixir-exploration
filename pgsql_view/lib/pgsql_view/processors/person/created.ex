defmodule PgsqlView.Processors.PersonCreated do
  alias PgsqlView.{Person, Repo}
  alias Ecto.Multi

  def process(payload) do
    Repo.get(Person, payload["id"])
    |> create_person(
      Map.merge(payload, %{
        "created_at" => payload["timestamp"],
        "updated_at" => payload["timestamp"]
      })
    )
  end

  defp create_person(_existing_person = %Person{}, _payload), do: Multi.new()

  defp create_person(_existing_person = nil, payload) do
    Multi.new()
    |> Multi.insert(:people, Person.changeset(%Person{}, payload))
  end
end
