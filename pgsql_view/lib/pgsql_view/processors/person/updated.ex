defmodule PgsqlView.Processors.PersonUpdated do
  alias PgsqlView.{Person, Repo}
  alias Ecto.Multi

  def process(payload) do
    Repo.get(Person, payload["id"])
    |> update_person(
      Map.merge(payload, %{
        "updated_at" => payload["timestamp"]
      })
    )
  end

  defp update_person(_existing_person = nil, _payload), do: Multi.new()

  defp update_person(existing_person = %Person{}, payload) do
    Multi.new()
    |> Multi.update(
      :update_person,
      Person.changeset(existing_person, payload)
    )
  end
end
