defmodule PgsqlView.Processors.Person do
  alias PgsqlView.Processors.{PersonCreated, PersonDeleted, PersonUpdated}

  def process_event(payload, "Elixir.Esys.Events.PersonCreated"),
    do: PersonCreated.process(payload)

  def process_event(payload, "Elixir.Esys.Events.PersonChangedFirstName"),
    do: PersonUpdated.process(payload)

  def process_event(payload, "Elixir.Esys.Events.PersonChangedLastName"),
    do: PersonUpdated.process(payload)

  def process_event(payload, "Elixir.Esys.Events.PersonChangedAge"),
    do: PersonUpdated.process(payload)

  def process_event(payload, "Elixir.Esys.Events.PersonDeleted"),
    do: PersonDeleted.process(payload)
end
