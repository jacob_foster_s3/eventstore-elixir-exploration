defmodule PgsqlView.Listeners.PersonListener do
  use Extreme.Listener
  alias PgsqlView.{EventMetadata, Persistor}

  defp get_last_event("people"), do: EventMetadata.get_last_event("people")
  defp get_last_event(_stream_name), do: -1

  defp process_push(push, stream_name = "people") do
    IO.inspect(push)
    event_number = push.event.event_number

    payload =
      Poison.decode!(push.event.data)
      |> Map.merge(%{"timestamp" => DateTime.from_unix!(push.event.created_epoch, :millisecond)})

    Persistor.persist(stream_name, event_number, payload, push.event.event_type)
    {:ok, event_number}
  end

  defp process_push(push, _stream_name), do: {:ok, push.event.event_number}
end
