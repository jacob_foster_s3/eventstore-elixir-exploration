defmodule PgsqlView.Repo do
  use Ecto.Repo,
    otp_app: :pgsql_view,
    adapter: Ecto.Adapters.Postgres
end
