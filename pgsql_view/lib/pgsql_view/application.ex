defmodule PgsqlView.Application do
  use Application

  @event_store PgsqlView.EventStore

  def start(_type, _args) do
    import Supervisor.Spec

    event_store_settings = Application.get_env(:extreme, :event_store)

    children = [
      PgsqlView.Repo,
      worker(Extreme, [event_store_settings, [name: @event_store]]),
      worker(PgsqlView.Listeners.PersonListener, [
        @event_store,
        "people",
        [name: PgsqlView.Listeners.PersonListener]
      ])
    ]

    opts = [strategy: :one_for_one, name: PgsqlView.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
