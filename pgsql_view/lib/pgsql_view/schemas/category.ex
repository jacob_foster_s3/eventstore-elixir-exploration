defmodule PgsqlView.Category do
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: false}

  schema "categories" do
    field(:name, :string)
    field(:created_at, :utc_datetime)
    field(:updated_at, :utc_datetime)
  end

  def changeset(category, params \\ %{}) do
    category
    |> Ecto.Changeset.cast(params, [:name, :created_at, :updated_at])
    |> Ecto.Changeset.validate_required([:name])
  end
end
