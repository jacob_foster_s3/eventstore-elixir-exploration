defmodule PgsqlView.EventMetadata do
  alias PgsqlView.Repo
  use Ecto.Schema

  @primary_key {:stream_name, :string, autoincrement: false}
  schema "event_metadata" do
    field(:last_event_id, :integer)
  end

  def changeset(%{stream_name: _, last_event_id: _} = params) do
    changeset(%__MODULE__{}, params)
  end

  def changeset(event_metadata, params \\ %{}) do
    event_metadata
    |> Ecto.Changeset.cast(params, [:stream_name, :last_event_id])
    |> Ecto.Changeset.validate_required([:stream_name, :last_event_id])
  end

  def get_last_event(stream_name) when is_bitstring(stream_name) do
    Repo.get_by(__MODULE__, stream_name: stream_name)
    |> get_last_event
  end

  def get_last_event(event_metadata = %__MODULE__{}), do: event_metadata.last_event_id
  def get_last_event(_event_metadata = nil), do: -1
end
