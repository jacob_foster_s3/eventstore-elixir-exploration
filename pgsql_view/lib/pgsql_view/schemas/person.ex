defmodule PgsqlView.Person do
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: false}

  schema "people" do
    field(:first_name, :string)
    field(:last_name, :string)
    field(:age, :integer)
    field(:created_at, :utc_datetime)
    field(:updated_at, :utc_datetime)
  end

  def changeset(person, params \\ %{}) do
    person
    |> Ecto.Changeset.cast(params, [:id, :first_name, :last_name, :age, :created_at, :updated_at])
    |> Ecto.Changeset.validate_required([:id, :first_name, :last_name])
  end
end
