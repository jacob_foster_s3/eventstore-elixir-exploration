use Mix.Config

config :pgsql_view, PgsqlView.Repo,
  database: "esys",
  username: "postgres",
  password: "root",
  hostname: "localhost"

config :pgsql_view, ecto_repos: [PgsqlView.Repo]

config :extreme, :protocol_version, 4

config :extreme, :event_store,
  db_type: :node,
  host: "localhost",
  port: 1113,
  username: "admin",
  password: "changeit",
  reconnect_delay: 2_000,
  mode: :write,
  connection_name: :esys,
  max_attempts: :infinity
