defmodule PgsqlView.Repo.Migrations.CreateEventMetadata do
  use Ecto.Migration

  def change do
    create table(:event_metadata, primary_key: false) do
      add(:stream_name, :string, primary_key: true)
      add(:last_event_id, :integer)
    end
  end
end
