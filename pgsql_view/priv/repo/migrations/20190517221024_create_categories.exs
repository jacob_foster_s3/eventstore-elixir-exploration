defmodule PgsqlView.Repo.Migrations.CreateCategories do
  use Ecto.Migration

  def change do
    create table(:categories, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:name, :string)

      add(:created_at, :utc_datetime)
      add(:updated_at, :utc_datetime)
    end
  end
end
