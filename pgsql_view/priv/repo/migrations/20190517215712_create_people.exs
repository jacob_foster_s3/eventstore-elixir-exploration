defmodule PgsqlView.Repo.Migrations.CreatePeople do
  use Ecto.Migration

  def change do
    create table(:people, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:first_name, :string)
      add(:last_name, :string)
      add(:age, :integer)

      add(:created_at, :utc_datetime)
      add(:updated_at, :utc_datetime)
    end
  end
end
