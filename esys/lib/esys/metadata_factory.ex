defmodule Esys.MetadataFactory do
  def get_metadata(event) do
    entityType =
      event.__struct__
      |> to_string
      |> String.split(".")
      |> List.last()
      |> Macro.underscore()
      |> String.split("_")
      |> List.first()
      |> String.capitalize()

    get_metadata(entityType, event)
  end

  # Only add entity metadata if we have an ID we are working with.
  def get_metadata(entityType, event = %{id: _}) do
    Poison.encode!(%{
      entityId: event.id,
      entityType: entityType
    })
  end

  # Otherwise, there's no metadata to capture.
  def get_metadata(entityType, event) do
    Poison.encode!(%{})
  end
end
