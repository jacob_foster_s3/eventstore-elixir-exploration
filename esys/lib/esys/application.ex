defmodule Esys.Application do
  use Application

  @event_store Esys.EventStore

  def start(_type, _args) do
    import Supervisor.Spec

    event_store_settings = Application.get_env(:extreme, :event_store)

    children = [
      worker(Extreme, [event_store_settings, [name: @event_store]])
    ]

    opts = [strategy: :one_for_one, name: Esys.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
