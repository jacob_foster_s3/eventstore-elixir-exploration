defmodule Esys.Events.CategoryCreated do
  @derive [Poison.Encoder]
  defstruct([:id, :name])
end
