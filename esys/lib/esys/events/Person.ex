defmodule Esys.Events.PersonCreated do
  @derive [Poison.Encoder]
  defstruct([:id, :first_name, :last_name, :age])
end

defmodule Esys.Events.PersonChangedFirstName do
  @derive [Poison.Encoder]
  defstruct([:id, :first_name])
end

defmodule Esys.Events.PersonChangedLastName do
  @derive [Poison.Encoder]
  defstruct([:id, :last_name])
end

defmodule Esys.Events.PersonChangedAge do
  @derive [Poison.Encoder]
  defstruct([:id, :age])
end

defmodule Esys.Events.PersonDeleted do
  @derive [Poison.Encoder]
  defstruct([:id])
end
