defmodule Esys do
  alias Esys.MetadataFactory
  alias Extreme.Msg, as: ExMsg

  def write_events(stream, events) do
    proto_events = Enum.map(events, &createProtoEvent/1)

    ExMsg.WriteEvents.new(
      event_stream_id: stream,
      expected_version: -2,
      events: proto_events,
      require_master: false
    )
  end

  def get_uuid() do
    Extreme.Tools.gen_uuid()
    |> UUID.binary_to_string!()
  end

  defp createProtoEvent(event) do
    ExMsg.NewEvent.new(
      event_id: Extreme.Tools.gen_uuid(),
      event_type: to_string(event.__struct__),
      data_content_type: 0,
      metadata_content_type: 0,
      data: Poison.encode!(event),
      metadata: Esys.MetadataFactory.get_metadata(event)
    )
  end
end
